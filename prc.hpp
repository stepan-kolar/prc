﻿/*
 * prc.hpp: Configuration for prc.cpp.
 * 
 * Copyright 2014 Štěpán Kolář
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/*  Parameters to configuration   */
#define SHORT_FORMAT_TIME       true    /* if unit is 0, don't show it                      */
#define NOT_ONLY_SECONDS        true    /* show minutes, hours, ...                         */
#define NEW_LINES             "\n\n"    /* show new lines in output                         */
#define MAX                        3    /* maximum units, they are defined in 'ti' and 'tc' */

/*           Time sizes           */
#define MIN_SEC                   60
#define HOUR_SEC                3600
#define DAY_SEC                86400

/*           Time units           */
#define MIN_UNIT                 'm'
#define HOUR_UNIT                'h'
#define DAY_UNIT                 'd'


#if NOT_ONLY_SECONDS == 1
	const int  ti[MAX] = {DAY_SEC,  HOUR_SEC,  MIN_SEC };//time units' size in seconds
	const char tc[MAX] = {DAY_UNIT, HOUR_UNIT, MIN_UNIT};//time units' shortcuts
#endif

