prg=prc
cpp=g++
cprs=xz
OBJ=${prg}.o
SRC=${prg}.cpp
HDR=${prg}.hpp
OPT=-O3


.PHONY: build
.PHONY: install
.PHONY: uninstall
.PHONY: clean
.PHONY: distrib


build: ${prg}

install: build
	cp ${prg} /usr/bin; \ 
	rm -f *.o ${prg}
	
uninstall:
	rm -f /usr/bin/${prg}
	
clean:
	rm -f *.o ${prg}
	
distrib:
	tar -c ${prg}* Makefile > ${prg}.tar; \
	${cprs} ${prg}.tar

${prg}: ${OBJ}
	${cpp} ${OBJ} -o ${prg} ${OPT}
${OBJ}: 
	${cpp} ${SRC} -c ${OPT}
