﻿/*
 * prc.cpp: Show what process returned and how took time.
 * 
 * Copyright 2014 Štěpán Kolář
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
#include <stdio.h>
#include <string.h>
#include <cstdlib>
#include <ctime>

#include "prc.hpp"


/*function for output*/
void out(int r, int t)
{
	printf(NEW_LINES"process returned: %d\ntime: ", r);

	//write short time
	if(t<1)
	{
		printf("<1 s\n");
		return;
	}

#if NOT_ONLY_SECONDS == true
	int tm[MAX], i;

	//recount to other units
	for(i=0; i<MAX; i++)
	{
		tm[i]=0;

		//recount unit
		if(t>=ti[i])
		{
			tm[i]=t/ti[i];
			t%=ti[i];
		}
	#if SHORT_FORMAT_TIME == true
		if(tm[i]>0)
	#endif
		printf("%d%c ", tm[i], tc[i]);//write unit
	}
#endif

#if SHORT_FORMAT_TIME == true
	if(t>0)
#endif
	//write seconds
	printf("%ds", t);
	printf("\n");
}


int main(int argc, char *argv[])
{
	int ret;
	int start, end;
	char cmd[1024]="";

	/*runnig process*/
	if(argc>1)
	{
		//rewrite arguments
		while(argc-->1)
		{
			strcat(cmd, *++argv);
			strcat(cmd, " ");
		}

		//run process and timekeeping
		start=time(0);
		ret=system(cmd);
		end=time(0)-start;

		out(ret>255?ret/256:ret, end);//ret/256 is for linux and ret is for windows
		return 0;
	}

	printf("Bad argument\n");
	return 1;
}
